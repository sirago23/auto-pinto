#!/usr/bin/env python

import rospy
from geometry_msgs.msg import Twist
from mavros_msgs.msg import *
from mavros_msgs.srv import *

class RcOverride:
	def __init__(self):
		self.max_pwm_v = rospy.get_param("/max_pwm_v")
		self.min_pwm_v = rospy.get_param("/min_pwm_v")

		self.max_pwm_w = rospy.get_param("/max_pwm_w")
		self.min_pwm_w = rospy.get_param("/min_pwm_w")

		self.sub = rospy.Subscriber("/cmd_vel", Twist , self.rc_override)

		self.pub = rospy.Publisher("/mavros/rc/override", OverrideRCIn , queue_size = 10)

		self.pwm_out_range = 400 #for servo 1,3 lim 1300 to 1700

	def rc_override(self, msg):
		v = msg.linear.x
		w = msg.angular.z

		v_pwm =  113640*v/self.pwm_out_range + 1500
		w_pwm = -113640*w/self.pwm_out_range + 1500

		if w_pwm > self.max_pwm_w:
			w_pwm = self.max_pwm_w
		elif w_pwm < self.min_pwm_w:
			w_pwm = self.min_pwm_w

		if v_pwm > self.max_pwm_v:
			v_pwm = self.max_pwm_v
		elif v_pwm < self.min_pwm_v:
			v_pwm = self.min_pwm_v

		rc_or = [w_pwm,0,v_pwm,0,0,0,0,0]
		self.pub.publish(rc_or)

class fcuModes:
    def __init__(self):
        pass

    def setArm(self):
        rospy.wait_for_service('mavros/cmd/arming')
        try:
            armService = rospy.ServiceProxy('mavros/cmd/arming', mavros_msgs.srv.CommandBool)
            armService(True)
        except rospy.ServiceException, e:
            print ("Service arming call failed: {}".format(e))

    def setDisarm(self):
        rospy.wait_for_service('mavros/cmd/arming')
        try:
            armService = rospy.ServiceProxy('mavros/cmd/arming', mavros_msgs.srv.CommandBool)
            armService(False)
        except rospy.ServiceException, e:
            print ("Service disarming call failed: {}".format(e))
    
    def setGuidedMode(self):
        rospy.wait_for_service('mavros/set_mode')
        try:
            flightModeService = rospy.ServiceProxy('mavros/set_mode', mavros_msgs.srv.SetMode)
            flightModeService(custom_mode='GUIDED')
        except rospy.ServiceException, e:
               print ("service set_mode call failed: {}. Guide Mode could not be set.".format(e))
    
    def setManualMode(self):
        rospy.wait_for_service('mavros/set_mode')
        try:
            flightModeService = rospy.ServiceProxy('mavros/set_mode', mavros_msgs.srv.SetMode)
            flightModeService(custom_mode='MANUAL')
        except rospy.ServiceException, e:
               print ("service set_mode call failed: {}. MANUAL Mode could not be set.".format(e))
    
    def setHoldMode(self):
        rospy.wait_for_service('mavros/set_mode')
        try:
            flightModeService = rospy.ServiceProxy('mavros/set_mode', mavros_msgs.srv.SetMode)
            flightModeService(custom_mode='HOLD')
        except rospy.ServiceException, e:
               print ("service set_mode call failed: {}. MANUAL Mode could not be set.".format(e))

    def setHome(self):
        rospy.wait_for_service('mavros/cmd/set_home')
        try:
            flightModeService = rospy.ServiceProxy('mavros/cmd/set_home', mavros_msgs.srv.CommandHome)
            current_gps = False
            yaw = 0.0
            latitude = 13.736638899999999
            longtitude = 100.5337898
            altitude = 0.0
            flightModeService(current_gps,latitude,longtitude,altitude)

        except rospy.ServiceException, e:
               print ("service set_home call failed: {}. Home could not be set.".format(e))
    
   
    def setExtNavMode(self):
        rospy.wait_for_service('mavros/param/set')
        try:
            flightModeService = rospy.ServiceProxy('mavros/param/set', mavros_msgs.srv.ParamSet)
            
            #the default) to use EKF2
            valueSet = ParamValue()
            valueSet.integer = 2
            valueSet.real = 0.0
            flightModeService(param_id ="AHRS_EKF_TYPE", value = valueSet)
            # the default to enable EK2
            valueSet.integer = 1
            flightModeService(param_id = "EK2_ENABLE", value = valueSet)
            # the default to disable EK3
            valueSet.integer = 0
            flightModeService(param_id = "EK3_ENABLE", value = valueSet)
            # to disable the GPS
            valueSet.integer = 0
            flightModeService(param_id = "GPS_TYPE", value = valueSet)
            # to disable the EKF use of the GPS
            valueSet.integer = 3
            flightModeService(param_id = "EK2_GPS_TYPE", value = valueSet)
            # unknown
            valueSet.integer = 0
            valueSet.real = 0.1
            flightModeService(param_id = "EK2_POSNE_M_NSE", value = valueSet)
            # unknown
            flightModeService(param_id = "EK2_VELD_M_NSE", value = valueSet)
            # unknown
            flightModeService(param_id = "EK2_VELNE_M_NSE", value = valueSet)
            #to disable the EKF use of the compass and instead rely on the heading from external navigation data.
            valueSet.integer = 0
            valueSet.real = 0.0
            #flightModeService(param_id = "MAG_ENABLE", value = valueSet)
            flightModeService(param_id = "COMPASS_USE", value = valueSet)
            flightModeService(param_id = "COMPASS_USE2", value = valueSet)
            flightModeService(param_id = "COMPASS_USE3", value = valueSet)

            #setup FCU param SYSID_MYGCS to match mavros system id (to accept control from mavros)
            valueSet.integer = 255
            flightModeService(param_id = "SYSID_MYGCS", value = valueSet)

        except rospy.ServiceException, e:
               print ("service set_param call failed: {}. Exnav could not be set.".format(e))

    def setDefaultNavMode(self):
        rospy.wait_for_service('mavros/param/set')
        try:
            flightModeService = rospy.ServiceProxy('mavros/param/set', mavros_msgs.srv.ParamSet)
            
            #the default) to use EKF2
            valueSet = ParamValue()
            valueSet.integer = 2
            valueSet.real = 0.0
            flightModeService(param_id ="AHRS_EKF_TYPE", value = valueSet)
            # the default to enable EK2
            valueSet.integer = 1
            flightModeService(param_id = "EK2_ENABLE", value = valueSet)
            # the default to disable EK3
            valueSet.integer = 0
            flightModeService(param_id = "EK3_ENABLE", value = valueSet)
            # to enable the GPS (default)
            valueSet.integer = 1
            flightModeService(param_id = "GPS_TYPE", value = valueSet)
            # to disable the EKF use of the GPS
            valueSet.integer = 1
            flightModeService(param_id = "EK2_GPS_TYPE", value = valueSet)
            # unknown
            valueSet.integer = 0
            valueSet.real = 1
            flightModeService(param_id = "EK2_POSNE_M_NSE", value = valueSet)
            # unknown
            valueSet.real = 0.7
            flightModeService(param_id = "EK2_VELD_M_NSE", value = valueSet)
            # unknown
            valueSet.real = 0.5
            flightModeService(param_id = "EK2_VELNE_M_NSE", value = valueSet)
            #Gyro noise
            valueSet.real = 0.03
            flightModeService(param_id = "EK2_GYRO_P_NSE", value = valueSet)
            #Accelerometer noise
            valueSet.real = 0.6
            flightModeService(param_id = "EK2_ACC_P_NSE", value = valueSet)
            #to disable the EKF use of the compass and instead rely on the heading from external navigation data.
            valueSet.integer = 1
            valueSet.real = 0.0
            #flightModeService(param_id = "MAG_ENABLE", value = valueSet)
            flightModeService(param_id = "COMPASS_USE", value = valueSet)
            flightModeService(param_id = "COMPASS_USE2", value = valueSet)
            valueSet.integer = 0
            flightModeService(param_id = "COMPASS_USE3", value = valueSet)

            #setup FCU param SYSID_MYGCS to match mavros system id (to accept control from mavros)
            valueSet.integer = 1 #255
            flightModeService(param_id = "SYSID_MYGCS", value = valueSet)

        except rospy.ServiceException, e:
               print ("service set_param call failed: {}. Exnav could not be set.".format(e))

if __name__ == '__main__':
	rospy.init_node('cmd_vel_2_override')
	
	mode = fcuModes()
	
	rate = rospy.Rate(20.0)

	mode.setHoldMode()

	mode.setDefaultNavMode()
	rate.sleep()
    
	mode.setHome()

	mode.setManualMode()

	mode.setArm()
	rate.sleep()

	try:
		RcOverride()
		rospy.spin()

	except rospy.ROSInterruptException:
		mode.setDisarm()

