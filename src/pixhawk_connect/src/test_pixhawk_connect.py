#!/usr/bin/env python
# ROS python API
import rospy

# 3D point & Stamped Pose msgs
from geometry_msgs.msg import Point, PoseStamped
# cmd_vel msg
from geometry_msgs.msg import TwistStamped
# import all mavros messages and services
from mavros_msgs.msg import *
from mavros_msgs.srv import *

# 3D point & Stamped geopose (for set_gp_origin)
from geographic_msgs.msg import GeoPointStamped, GeoPoint

#
# Flight modes class
# Flight modes are activated using ROS services
class fcuModes:
    def __init__(self):
        pass

    def setArm(self):
        rospy.wait_for_service('mavros/cmd/arming')
        try:
            armService = rospy.ServiceProxy('mavros/cmd/arming', mavros_msgs.srv.CommandBool)
            armService(True)
        except rospy.ServiceException, e:
            print ("Service arming call failed: {}".format(e))

    def setDisarm(self):
        rospy.wait_for_service('mavros/cmd/arming')
        try:
            armService = rospy.ServiceProxy('mavros/cmd/arming', mavros_msgs.srv.CommandBool)
            armService(False)
        except rospy.ServiceException, e:
            print ("Service disarming call failed: {}".format(e))
    
    def setGuidedMode(self):
        rospy.wait_for_service('mavros/set_mode')
        try:
            flightModeService = rospy.ServiceProxy('mavros/set_mode', mavros_msgs.srv.SetMode)
            flightModeService(custom_mode='GUIDED')
        except rospy.ServiceException, e:
               print ("service set_mode call failed: {}. Guide Mode could not be set.".format(e))
    
    def setManualMode(self):
        rospy.wait_for_service('mavros/set_mode')
        try:
            flightModeService = rospy.ServiceProxy('mavros/set_mode', mavros_msgs.srv.SetMode)
            flightModeService(custom_mode='MANUAL')
        except rospy.ServiceException, e:
               print ("service set_mode call failed: {}. MANUAL Mode could not be set.".format(e))
    
    def setHoldMode(self):
        rospy.wait_for_service('mavros/set_mode')
        try:
            flightModeService = rospy.ServiceProxy('mavros/set_mode', mavros_msgs.srv.SetMode)
            flightModeService(custom_mode='HOLD')
        except rospy.ServiceException, e:
               print ("service set_mode call failed: {}. MANUAL Mode could not be set.".format(e))

    def setHome(self):
        rospy.wait_for_service('mavros/cmd/set_home')
        try:
            flightModeService = rospy.ServiceProxy('mavros/cmd/set_home', mavros_msgs.srv.CommandHome)
            current_gps = False
            yaw = 0.0
            latitude = 13.736638899999999
            longtitude = 100.5337898
            altitude = 0.0
            flightModeService(current_gps,latitude,longtitude,altitude)

        except rospy.ServiceException, e:
               print ("service set_home call failed: {}. Home could not be set.".format(e))
    
   
    def setExtNavMode(self):
        rospy.wait_for_service('mavros/param/set')
        try:
            flightModeService = rospy.ServiceProxy('mavros/param/set', mavros_msgs.srv.ParamSet)
            
            #the default) to use EKF2
            valueSet = ParamValue()
            valueSet.integer = 2
            valueSet.real = 0.0
            flightModeService(param_id ="AHRS_EKF_TYPE", value = valueSet)
            # the default to enable EK2
            valueSet.integer = 1
            flightModeService(param_id = "EK2_ENABLE", value = valueSet)
            # the default to disable EK3
            valueSet.integer = 0
            flightModeService(param_id = "EK3_ENABLE", value = valueSet)
            # to disable the GPS
            valueSet.integer = 0
            flightModeService(param_id = "GPS_TYPE", value = valueSet)
            # to disable the EKF use of the GPS
            valueSet.integer = 3
            flightModeService(param_id = "EK2_GPS_TYPE", value = valueSet)
            # unknown
            valueSet.integer = 0
            valueSet.real = 0.1
            flightModeService(param_id = "EK2_POSNE_M_NSE", value = valueSet)
            # unknown
            flightModeService(param_id = "EK2_VELD_M_NSE", value = valueSet)
            # unknown
            flightModeService(param_id = "EK2_VELNE_M_NSE", value = valueSet)
            #to disable the EKF use of the compass and instead rely on the heading from external navigation data.
            valueSet.integer = 0
            valueSet.real = 0.0
            #flightModeService(param_id = "MAG_ENABLE", value = valueSet)
            flightModeService(param_id = "COMPASS_USE", value = valueSet)
            flightModeService(param_id = "COMPASS_USE2", value = valueSet)
            flightModeService(param_id = "COMPASS_USE3", value = valueSet)

            #setup FCU param SYSID_MYGCS to match mavros system id (to accept control from mavros)
            valueSet.integer = 255
            flightModeService(param_id = "SYSID_MYGCS", value = valueSet)

        except rospy.ServiceException, e:
               print ("service set_param call failed: {}. Exnav could not be set.".format(e))

    def setDefaultNavMode(self):
        rospy.wait_for_service('mavros/param/set')
        try:
            flightModeService = rospy.ServiceProxy('mavros/param/set', mavros_msgs.srv.ParamSet)
            
            #the default) to use EKF2
            valueSet = ParamValue()
            valueSet.integer = 2
            valueSet.real = 0.0
            flightModeService(param_id ="AHRS_EKF_TYPE", value = valueSet)
            # the default to enable EK2
            valueSet.integer = 1
            flightModeService(param_id = "EK2_ENABLE", value = valueSet)
            # the default to disable EK3
            valueSet.integer = 0
            flightModeService(param_id = "EK3_ENABLE", value = valueSet)
            # to enable the GPS (default)
            valueSet.integer = 1
            flightModeService(param_id = "GPS_TYPE", value = valueSet)
            # to disable the EKF use of the GPS
            valueSet.integer = 1
            flightModeService(param_id = "EK2_GPS_TYPE", value = valueSet)
            # unknown
            valueSet.integer = 0
            valueSet.real = 1
            flightModeService(param_id = "EK2_POSNE_M_NSE", value = valueSet)
            # unknown
            valueSet.real = 0.7
            flightModeService(param_id = "EK2_VELD_M_NSE", value = valueSet)
            # unknown
            valueSet.real = 0.5
            flightModeService(param_id = "EK2_VELNE_M_NSE", value = valueSet)
            #Gyro noise
            valueSet.real = 0.03
            flightModeService(param_id = "EK2_GYRO_P_NSE", value = valueSet)
            #Accelerometer noise
            valueSet.real = 0.6
            flightModeService(param_id = "EK2_ACC_P_NSE", value = valueSet)
            #to disable the EKF use of the compass and instead rely on the heading from external navigation data.
            valueSet.integer = 1
            valueSet.real = 0.0
            #flightModeService(param_id = "MAG_ENABLE", value = valueSet)
            flightModeService(param_id = "COMPASS_USE", value = valueSet)
            flightModeService(param_id = "COMPASS_USE2", value = valueSet)
            valueSet.integer = 0
            flightModeService(param_id = "COMPASS_USE3", value = valueSet)

            #setup FCU param SYSID_MYGCS to match mavros system id (to accept control from mavros)
            valueSet.integer = 1 #255
            flightModeService(param_id = "SYSID_MYGCS", value = valueSet)

        except rospy.ServiceException, e:
               print ("service set_param call failed: {}. Exnav could not be set.".format(e))

class Controller:
    # initialization method
    def __init__(self):
        # rover state
        self.state = State()
        # Instantiate a setpoints message
        self.sp = PositionTarget()
        # Instantiate a setpoints_velocity message
        self.sp_vel = TwistStamped()
        # set the flag to use position setpoints and yaw angle
        self.sp.type_mask = int('010111111000', 2)
        # LOCAL_NED
        self.sp.coordinate_frame = 1

        # Step size for position update
        self.STEP_SIZE = 2.0
		# Fence. We will assume a square fence for now
        self.FENCE_LIMIT = 5.0

        # A Message for the current local position of the drone
        self.local_pos = Point(0.0, 0.0, 3.0)

        # initial values for setpoints
        self.sp.position.x = 0.0
        self.sp.position.y = 0.0
        self.sp.position.z = 0.0
        self.sp.yaw = 0.0

        #initialize counter for setpoint velocity
        self.counter = 0
        #initialize values for setpoints_velocity
        self.sp_vel.header.stamp = rospy.Time.now()
        self.sp_vel.header.seq = 0
        self.sp_vel.twist.linear.x = 0
        self.sp_vel.twist.linear.y = 0
        self.sp_vel.twist.linear.z = 0
        self.sp_vel.twist.angular.x = 0
        self.sp_vel.twist.angular.y = 0
        self.sp_vel.twist.angular.z = 0

        #initialize vision_pose feedback (test)
        self.vision_pose = PoseStamped()
        self.vision_pose.header.stamp = rospy.Time.now()
        self.vision_pose.header.seq = 0
        self.vision_pose.pose.position.x = 1
        self.vision_pose.pose.position.y = 0
        self.vision_pose.pose.position.z = 0
        self.vision_pose.pose.orientation.x = 0
        self.vision_pose.pose.orientation.y = 0
        self.vision_pose.pose.orientation.z = 0
        self.vision_pose.pose.orientation.w = 1

        #initialize set_global_origin (@Chula)
        self.global_origin = GeoPointStamped()
        self.global_origin.header.stamp = rospy.Time.now()
        self.global_origin.header.seq = 0
        self.global_origin.position.latitude = 13.736638899999999
        self.global_origin.position.longitude = 100.5337898
        self.global_origin.position.altitude= 0
        
        # speed of the drone is set using MPC_XY_CRUISE parameter in MAVLink
        # using QGroundControl. By default it is 5 m/s.

    # set desired velocity
    def setpoint_velocity(self,V,omega):
        self.sp_vel.header.stamp = rospy.Time.now()
        self.sp_vel.header.seq = self.counter
        self.sp_vel.twist.linear.x = V
        self.sp_vel.twist.linear.y = 0
        self.sp_vel.twist.linear.z = 0
        self.sp_vel.twist.angular.x = 0
        self.sp_vel.twist.angular.y = 0
        self.sp_vel.twist.angular.z = omega
        # run a counter 
        #self.counter = self.counter + 1

    # send vision_pose to fcu (test)
    def set_vision_pose(self):
        self.vision_pose.header.stamp = rospy.Time.now()
        self.vision_pose.header.seq = self.counter
        self.vision_pose.pose.position.x = 1
        self.vision_pose.pose.position.y = 0
        self.vision_pose.pose.position.z = 0
        self.vision_pose.pose.orientation.x = 0
        self.vision_pose.pose.orientation.y = 0
        self.vision_pose.pose.orientation.z = 0
        self.vision_pose.pose.orientation.w = 1
        # run a counter 
        self.counter = self.counter + 1

	# Callbacks

    ## local position callback
    def posCb(self, msg):
        self.local_pos.x = msg.pose.position.x
        self.local_pos.y = msg.pose.position.y
        self.local_pos.z = msg.pose.position.z

    ## Drone State callback
    def stateCb(self, msg):
        self.state = msg
        print(self.state)

    ## Update setpoint message
    def updateSp(self):
        self.sp.position.x = self.local_pos.x
        self.sp.position.y = self.local_pos.y

    def x_dir(self):
    	self.sp.position.x = self.local_pos.x + 5
    	self.sp.position.y = self.local_pos.y

    def neg_x_dir(self):
    	self.sp.position.x = self.local_pos.x - 5
    	self.sp.position.y = self.local_pos.y

    def y_dir(self):
    	self.sp.position.x = self.local_pos.x
    	self.sp.position.y = self.local_pos.y + 5

    def neg_y_dir(self):
    	self.sp.position.x = self.local_pos.x
    	self.sp.position.y = self.local_pos.y - 5

    


# Main function
def main():

    # initiate node
    rospy.init_node('setpoint_node', anonymous=True)

    # flight mode object
    modes = fcuModes()

    # controller object
    cnt = Controller()

    # ROS loop rate
    rate = rospy.Rate(20.0)

    # Subscribe to rover state
    rospy.Subscriber('mavros/state', State, cnt.stateCb)

    # Subscribe to rover's local position
    rospy.Subscriber('mavros/local_position/pose', PoseStamped, cnt.posCb)

    # Subscribe to rover's local position
    #rospy.Subscriber('/position_target', PoseStamped, cnt.postargetCb)

    # Setpoint publisher    
    #sp_pub = rospy.Publisher('mavros/setpoint_raw/local', PositionTarget, queue_size=1)

    # Setpoint_velocity publisher    
    sp_pub_vel = rospy.Publisher('mavros/setpoint_velocity/cmd_vel', TwistStamped, queue_size=1)

    # Vision position publisher ( test)
    vision_pos_pub = rospy.Publisher('mavros/vision_pose/pose', PoseStamped, queue_size=1)

    # Set_gp_origin publisher
    gp_origin_pub = rospy.Publisher('/mavros/global_position/set_gp_origin', GeoPointStamped, queue_size=1)

    #set default mode to HOLD
    modes.setHoldMode()

    #set to use external nav instaed of GPS
    #modes.setExtNavMode()
    
    #set to use defualt nav mode
    modes.setDefaultNavMode()
    rate.sleep()

    #Set gp origin & new home
    gp_origin_pub.publish(cnt.global_origin)
    modes.setHome()

    '''
    #rate.sleep()
    # We need to send few setpoint messages, then activate GUIDED mode, to take effect
    k=0
    while k<10:
        #sp_pub.publish(cnt.sp)
        # publish feedback pose (test)
        cnt.set_vision_pose()
        vision_pos_pub.publish(cnt.vision_pose)
        #print(vision_pos_pub)
        #cnt.setpoint_velocity(V = 0, omega = 0)
        #sp_pub_vel.publish(cnt.sp_vel)
        rate.sleep()
        k = k + 1

    # activate GUIDED mode
    #modes.setOffboardMode()
    modes.setGuidedMode()

    # Make sure the rover is armed
    '''

    while not cnt.state.armed:
        modes.setArm()
        rate.sleep()
    
    # activate MANUAL mode
    modes.setManualMode()

    # ROS main loop
    while not rospy.is_shutdown():
    	#cnt.updateSp()
    	#sp_pub.publish(cnt.sp)

        #cnt.set_vision_pose()
        #vision_pos_pub.publish(cnt.vision_pose)
        #print(vision_pos_pub)
        #set velocity
        #cnt.setpoint_velocity(V = 0.0, omega = 0.0)
        #sp_pub_vel.publish(cnt.sp_vel)
    	rate.sleep()
    
    # disarm and reset parameter when shutdown
    #cnt.setpoint_velocity(V = 0.0, omega = 0.0)
    #sp_pub_vel.publish(cnt.sp_vel)
    #modes.setDisarm()
    #rate.sleep()
    

if __name__ == '__main__':
	try:
		main()
	except rospy.ROSInterruptException:
		pass
